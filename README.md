# BRAINTEASER Website
Version 2.0 of the BTO Website. Version 1.0 is available at https://bitbucket.org/brainteaser-health/site/src/V1/

Repository containing the files composing the Brainteaser website. 

The website provides the documentation for the Brainteaser ontology, which aims at modeling the [two diseases](https://www.webmd.com/multiple-sclerosis/ms-or-als) Amyotrophic Lateral Sclerosis (ALS) and Multiple Sclerosis (MS).

The website is available at the following link [http://w3id.org/brainteaser/ontology](http://w3id.org/brainteaser/ontology).

The documentation includes information about the medical background of the two diseases; a [WebVOWL](http://vowl.visualdataweb.org/webvowl.html) graphical representation of the ontology; the design approach; a high-level description of the ontology and its part, and the in-depth documentation of each class and named individual in the ontology.

The website is created automatically using [Widoco](https://github.com/dgarijo/Widoco/). 

## URL Mappings
For the URL mappings we used the service provided by [w3id.org](https://w3id.org/). The rules we set can be found the in [w3id.org GitHub](https://github.com/perma-id/w3id.org) repository, in the sub-directory "brainteaser". If we need to update the these rules, a pull request to this main repository is needed.


### License ###
All the contents of this repository are shared using the [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
![CC logo](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

### Acknowledgements ###
This work has been supported by the [BRAINTEASER](https://brainteaser.health/)  project,  funded by the European Union's Horizon 2020 research and innovation programme under the grant agreement No GA101017598.