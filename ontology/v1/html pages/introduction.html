<h2 id="intro" class="list">Introduction 
      <span class="backlink"> back to <a href="#toc">ToC</a></span>
</h2>

<span class="markdown">

      <p>This webpage describes the design and development of the <strong>BRAINTEASER Ontology</strong> (BO) whose purpose is to jointly model both <em>Amyotrophic Lateral Sclerosis (ALS)</em> and <em>Multiple Sclerosis (MS)</em>.</p>

      <p>The BO serves multiple purposes:</p>
      <ul>
            <li>to provide a unified and comprehensive conceptual view about ALS and MS, which are typically dealt with separately, allowing us to coherently integrate the data coming from the different medical partners in the project;</li>
            <li>to seamlessly represent both <em>retrospective</em> data and <em>prospective</em> data, produced during the lifetime of BRAINTEASER</li>
            <li>to allow for sharing and re-using the BRAINTEASER datasets according to Open Science and FAIR principles.</li>
      </ul>

      <h3>BRAINTEASER Ontology approach</h3>
      <p>The BO is innovative since it relies on very few seed concepts - <span class=verb>Patient, Clinical Trial, Disease, Event</span> - which allow us to jointly model ALS and MS and to grasp the time dimension entailed by the progression of such diseases.</p>

      <p>Indeed, the core idea is that a <span class=verb>Patient</span> participates in a <span class=verb>Clinical Trial</span>, suffers from some <span class=verb>Diseases</span>, and undergoes <span class=verb>Event</span>s. These <span class=verb>Event</span>s are different in nature and cover a wide range of cases, e.g. <span class=verb>Onset, Pregnancy, Symptom, Trauma, Diagnostic Procedure</span> (like evoked potentials or ALS-FRS questionnaires) <span class=verb>Therapeutic Procedure</span> (like <span class=verb>Mechanical Ventilation</span> for ALS or <span class=verb>Disease-Modifying Therapy</span> for MS), <span class=verb>Relapse</span>, and more. 
      Overall, this event-based approach allows us to model ALS and MS in an unified way, sharing concepts among these two diseases, and to track what happens during their progression. Details of the design and functioning of the BO are provided in the next sections of this deliverable.</p>

      <h3>Role of the BRAINTEASER Ontology</h3>
      <p>The BO plays an important role in the overall BRAINTEASER architecture, shown in Figure 1. Indeed, it informs the implementation of the <em>BRAINTEASER Semantic Data Cloud</em> since the data contained here will be represented according to the BO, i.e., they will be an instance of the BO.</p>

      <p>In Figure 1 it is possible to see that all the data will be anonymized prior to being represented in the BO. This holds true for both the <em>retrospective data</em>, i.e., the data already held by clinical partners on the right of the figure, and the prospective data, i.e., the new data that will be collected during the project lifetime on the left.</p>

      <figure>
            <img src="img/architecture.png" alt="Brainteaser architecture" style="display: block; margin-left: auto; margin-right: auto; width: 60%;">
            <center>
                  <figcaption><b>Figure 1.</b>The BRAINTEASER ontology and its role in the overall BRAINTEASER architecture.
            </figcaption>
      </center>
      </figure>

      <p>The data held in the BRAINTEASER Semantic Data Cloud, exported in a suitable format, will then be used to train the AI models needed to predict the progression of both ALS and MS.</p>

      <p>Finally, a subset of the data in the BRAINTEASER Semantic Data Cloud will be exported to the European Open Science Cloud (EOSC) and they will be also shared and exploited for the Open Evaluation Challenges.</p>

      <p>Figure 2 shows an overall picture of the whole data flow associated with the BO and the BRAINTEASER Semantic Data Cloud, also in relation to Open Evaluation Challenges and the EOSC.</p>

      <figure>
            <img src="img/dataflow.png" alt="Data flow" style="display: block; margin-left: auto; margin-right: auto; width: 60%;">
            <center>
                  <figcaption><b>Figure 2.</b>The overall data flow associated with the BO and the BRAINTEASER Semantic Data Cloud.
            </figcaption>
      </center>
      </figure>

      <p>In summary, the BO ontology will model the following data sources:</p>

      <ul>
            <li><em>“Raw” data [anonymized]</em>: these are the retrospective and prospective data discussed above:
                  <ul>
                        <li>Clinical data</li>
                        <li>Sensor/App data</li>
                  </ul>
            </li>
            <li>
                  <em>Generated data</em>: once trained on the above data, the AI models can be serialised as well and become part of the modelled data.
                  <ul>
                        <li>Serialized AI models</li>
                  </ul>
            </li>
            <li><em>Evaluation Challenges</em>: they will rely on three kinds of data which become part of the modelled data:</li>
            <ul>
                  <li>Evaluation Corpora: these are the training/validation/test sets and they are obtained by selecting an appropriate subset of the “raw” data;</li>
                  <li>AI models outputs: these are the results produced by the participating systems;</li>
                  <li>Performance scores and statistical analyses: the results produced by participants will be scored and then statistical analyses will be computed to assess them.</li>
            </ul>
      </ul>

      <p>The BO will also allow to link these data with other resources available in the <a href="https://lod-cloud.net/">Linked Open Data Cloud</a>.</p>


</span>